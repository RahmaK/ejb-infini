package tn.esprit.ejbinfini.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;

/**
 * 
 * The <b>"@Embeddable"</b> annotation specifies a class whose instances are stored as a part
 * of an owning entity and share the identity of the entity. No need to declare
 * an id into an embeddable class. Each of the persistent properties or fields
 * of the embedded object is mapped to the database table for the entity. Note
 * that the Transient annotation may be used to designate the non-persistent
 * state of an embeddable class.
 * <p>
 * 
 * @author Amine Bessrour
 *
 */

@Embeddable
public class Address implements Serializable {

	private String street;
	private String city;
	private Integer zipCode;
	private String country;

	public Address() {
		super();
	}

	public Address(String street, String city, Integer zipCode, String country) {
		super();
		this.street = street;
		this.city = city;
		this.zipCode = zipCode;
		this.country = country;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getZipCode() {
		return zipCode;
	}

	public void setZipCode(Integer zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	private static final long serialVersionUID = 1L;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		result = prime * result + ((zipCode == null) ? 0 : zipCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (street == null) {
			if (other.street != null)
				return false;
		} else if (!street.equals(other.street))
			return false;
		if (zipCode == null) {
			if (other.zipCode != null)
				return false;
		} else if (!zipCode.equals(other.zipCode))
			return false;
		return true;
	}

}
