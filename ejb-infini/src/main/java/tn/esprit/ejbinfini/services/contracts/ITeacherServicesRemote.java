package tn.esprit.ejbinfini.services.contracts;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.ejbinfini.entities.Teacher;
import tn.esprit.ejbinfini.exceptions.MoreThanOneResultException;

@Remote
public interface ITeacherServicesRemote {
	public void createTeacher(Teacher teacher);

	public void updateTeacher(Teacher teacher);

	public void deleteTeacher(Teacher teacher);

	public void deleteTeacher(Integer id);

	public Teacher retrieveTeacher(Integer id);

	public Teacher retrieveTeacherByEmailAndPassword(String email,
			String password) throws MoreThanOneResultException;
	
	public boolean retrieveTeacherByEmail(String email);

	public Teacher retrieveTeacherByFirstName(String fName)
			throws MoreThanOneResultException;

	public List<Teacher> retrieveAllTeachers();
}
