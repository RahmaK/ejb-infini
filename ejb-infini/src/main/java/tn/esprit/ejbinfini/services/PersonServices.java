package tn.esprit.ejbinfini.services;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.ejbinfini.entities.Person;
import tn.esprit.ejbinfini.exceptions.MoreThanOneResultException;
import tn.esprit.ejbinfini.services.contracts.IPersonServicesLocal;
import tn.esprit.ejbinfini.services.contracts.IPersonServicesRemote;

@Stateless
public class PersonServices implements IPersonServicesRemote,
		IPersonServicesLocal {

	@PersistenceContext(unitName = "ejb-infini")
	EntityManager em;

	public PersonServices() {
	}

	@Override
	public Person retrievePersonByEmailAndPassword(String email, String password)
			throws MoreThanOneResultException {
		TypedQuery<Person> query = em
				.createQuery(
						"select p from Person p where p.email =:email and p.password =:password",
						Person.class);
		query.setParameter("email", email);
		query.setParameter("password", password);
		try {
			return query.getSingleResult();
		} catch (NonUniqueResultException e) {
			throw new MoreThanOneResultException();
		} catch (NoResultException e) {
			throw new MoreThanOneResultException();
		}
	}

}
