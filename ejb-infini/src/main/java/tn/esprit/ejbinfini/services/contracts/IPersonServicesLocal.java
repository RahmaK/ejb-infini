package tn.esprit.ejbinfini.services.contracts;

import javax.ejb.Local;

import tn.esprit.ejbinfini.entities.Person;
import tn.esprit.ejbinfini.exceptions.MoreThanOneResultException;

@Local
public interface IPersonServicesLocal {
	
	public Person retrievePersonByEmailAndPassword(String email,
			String password) throws MoreThanOneResultException;
}
