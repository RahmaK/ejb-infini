package tn.esprit.ejbinfini.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.ejbinfini.entities.Teacher;
import tn.esprit.ejbinfini.exceptions.MoreThanOneResultException;
import tn.esprit.ejbinfini.services.contracts.ITeacherServicesLocal;
import tn.esprit.ejbinfini.services.contracts.ITeacherServicesRemote;

/**
 * Session Bean implementation class TeacherServices
 */
@Stateless
public class TeacherServices implements ITeacherServicesRemote,
		ITeacherServicesLocal {

	@PersistenceContext(unitName = "ejb-infini")
	private EntityManager em;

	/**
	 * This service adds a teacher considered as new to the peristence context.
	 * Note that persist doesn't add the teacher to the database. The entity
	 * manager rather stores it as managed or attached to the active persistence
	 * context
	 * 
	 */
	@Override
	public void createTeacher(Teacher teacher) {
		em.persist(teacher);
	}

	/**
	 * This service adds a teacher to the peristence context. Merge doesn't
	 * insert/update the teacher into the database. The entity manager rather
	 * stores it as managed or attached to the active persistence context. When
	 * the entity manager comes to flush it synchronizes with the database
	 * 
	 */
	@Override
	public void updateTeacher(Teacher teacher) {
		em.merge(teacher);
	}

	/**
	 * This service marks a teacher being managed in the active peristence
	 * context as removed. Thus, delete doesn't remove the teacher from the
	 * database. When the entity manager comes to flush it synchronizes with the
	 * database. If the entity is in context and labeled as "removed", it will
	 * be then removed form the database.
	 * 
	 * N.B Can't mark as "removed" an entity not attached to the context.
	 * 
	 */
	@Override
	public void deleteTeacher(Teacher teacher) {
		em.remove(em.find(Teacher.class, teacher.getId()));
	}

	/**
	 * This service marks a teacher being managed in the active peristence
	 * context as removed. Thus, delete doesn't remove the teacher from the
	 * database. When the entity manager comes to flush it synchronizes with the
	 * database. If the entity is in context and labeled as "removed", it will
	 * be then removed form the database.
	 * 
	 * N.B Can't mark as "removed" an entity not attached to the context.
	 * 
	 */
	@Override
	public void deleteTeacher(Integer id) {
		em.remove(em.find(Teacher.class, id));
	}

	/**
	 * This service fetchs by id a teacher from the database and puts it in the
	 * active peristence context.
	 * 
	 * N.B Can't mark as "removed" an entity not attached to the context.
	 * 
	 */
	@Override
	public Teacher retrieveTeacher(Integer id) {
		return em.find(Teacher.class, id);
	}

	/**
	 * This services uses JPQL to ask the entity manager to fetch a teacher by
	 * fName. JPQL stands for JPA Query Language and is build upon SQL. It
	 * questions JPA Entity rather than the database itself.
	 * 
	 * Below a TypedQuery is used rather than raw JPQL query or Native SQL
	 * query. The entity manager instanciates the query object via
	 * "em.createQuery" and the query string. A parameter is added via
	 * "query.setParameter". Finally result fetching can be tuned via the object
	 * query. Below we are expecting a unique result. Thus we're using
	 * "getSingleResult" which returns the unique result, null if any or an
	 * exception if more than one.
	 * 
	 */
	@Override
	public Teacher retrieveTeacherByFirstName(String fName)
			throws MoreThanOneResultException {
		TypedQuery<Teacher> query = em.createQuery(
				"select s from Teacher s where s.fName =:fName", Teacher.class);
		query.setParameter("fName", fName);
		try {
			return query.getSingleResult();
		} catch (NonUniqueResultException e) {
			throw new MoreThanOneResultException();
		}
	}

	/**
	 * This services uses JPQL to ask the entity manager to fetch a teacher by
	 * fName. JPQL stands for JPA Query Language and is build upon SQL. It
	 * questions JPA Entity rather than the database itself.
	 * 
	 * Below a TypedQuery is used rather than raw JPQL query or Native SQL
	 * query. The entity manager instanciates the query object via
	 * "em.createQuery" and the query string. A parameter is added via
	 * "query.setParameter". Finally result fetching can be tuned via the object
	 * query. Below we are fetch the whole resultSet.
	 */
	@Override
	public List<Teacher> retrieveAllTeachers() {
		TypedQuery<Teacher> query = em.createQuery("select s from Teacher s",
				Teacher.class);
		return query.getResultList();
	}

	@Override
	public Teacher retrieveTeacherByEmailAndPassword(String email,
			String password) throws MoreThanOneResultException {
		TypedQuery<Teacher> query = em
				.createQuery(
						"select s from Teacher s where s.email =:email and s.password =:password",
						Teacher.class);
		query.setParameter("email", email);
		query.setParameter("password", password);
		try {
			return query.getSingleResult();
		} catch (NonUniqueResultException e) {
			throw new MoreThanOneResultException();
		} catch (NoResultException e) {
			throw new MoreThanOneResultException();
		}
	}

	@Override
	public boolean retrieveTeacherByEmail(String email) {
		TypedQuery<Teacher> query = em.createQuery(
				"select s from Teacher s where s.email =:email", Teacher.class);
		query.setParameter("email", email);
		return query.getResultList().size() > 0;
	}

}
