package tn.esprit.ejbinfini.services.contracts;

import javax.ejb.Remote;

import tn.esprit.ejbinfini.entities.Person;
import tn.esprit.ejbinfini.exceptions.MoreThanOneResultException;

@Remote
public interface IPersonServicesRemote {
	
	public Person retrievePersonByEmailAndPassword(String email,
			String password) throws MoreThanOneResultException;
}
